import React from "react";

interface Props {
  index: number
}

export const ThankYouContent = ({index}: Props) => {
  return (
    <>
      <div className="thank-you-container">
        <img src="/illustration-thank-you.svg"/>
        <span className="selected-rating">You selected {index} out of 5</span>
        <h1>Thank you!</h1>
        <p>{"We appreciate you taking the time to give a rating. If you ever need more support, don't hesitate to get in touch!"}</p>
      </div>
    </>
  )
}
