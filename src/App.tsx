import './App.scss'
import React from 'react';
import {InteractiveRating} from './InteractiveRating';
const App = () => {
  return (
    <div className="App">
      <InteractiveRating/>
    </div>
  )
}

export default App
