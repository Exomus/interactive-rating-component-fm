import React from 'react';

interface Props {
  currentIndex: number,
  setIndex: (_: number) => void,
  setSubmitted: (_: boolean) => void
}

export const RatingContent = ({currentIndex, setIndex, setSubmitted}: Props) => {
  return (
    <>
      <div className="star-container">
        <img src="/icon-star.svg" alt="star icon"/>
      </div>
      <h1>How did we do?</h1>
      <p>Please let us know how we did with your support request. All feedback is appreciated to help us improve our
        offering</p>
      <ul>
        <li>
          <button className={currentIndex === 1 ? 'button-selected' : null} onClick={() => setIndex(1)}>1</button>
        </li>
        <li>
          <button className={currentIndex === 2 ? 'button-selected' : null} onClick={() => setIndex(2)}>2</button>
        </li>
        <li>
          <button className={currentIndex === 3 ? 'button-selected' : null} onClick={() => setIndex(3)}>3</button>
        </li>
        <li>
          <button className={currentIndex === 4 ? 'button-selected' : null} onClick={() => setIndex(4)}>4</button>
        </li>
        <li>
          <button className={currentIndex === 5 ? 'button-selected' : null} onClick={() => setIndex(5)}>5</button>
        </li>
      </ul>
      <button className="cta" onClick={() => setSubmitted(true)} disabled={currentIndex === 0}>Submit</button>
    </>
  )
}
