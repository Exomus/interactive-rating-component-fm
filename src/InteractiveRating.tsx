import React, {useState} from 'react';
import {RatingContent} from './RatingContent';
import {ThankYouContent} from './ThankYouContent';

export const InteractiveRating = () => {

  let [submitted, setSubmitted] = useState(false);
  let [index, setIndex] = useState(0);

  const content = !submitted ? <RatingContent currentIndex={index} setIndex={setIndex} setSubmitted={setSubmitted}/> :
    <ThankYouContent index={index}/>
  return (
    <>
      <div className="card">
        <div className="card-content">
          {content}
        </div>
      </div>
    </>
  )
}
